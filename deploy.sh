kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
helm upgrade --install ingres-nginx ingress-nginx  --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace --set controller.hostNetwork=true --set controller.hostPort.enabled=true --set controller.service.type=NodePort --set dnsPolicy=ClusterFirstWithHostNet --set controller.kind=DaemonSet   
sleep 60
helm upgrade --install prom kube-prometheus-stack --repo https://prometheus-community.github.io/helm-charts -n monitoring --create-namespace --values submission/monitoring/prometheus-values.yaml
helm upgrade --install promtail promtail --repo https://grafana.github.io/helm-charts -f submission/monitoring/promtail-values.yaml -n monitoring
helm upgrade --install loki loki-distributed  --repo https://grafana.github.io/helm-charts -n monitoring
helm upgrade --install consul consul  --repo  https://helm.releases.hashicorp.com  --create-namespace -n consul --values submission/service_mesh/consul-values.yaml
kubectl apply -f submission/

echo "Done"

