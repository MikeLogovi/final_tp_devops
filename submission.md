# Description de la remise

### Nom complet: Babou Laris                      
### NIP: 536951333

### Nom complet: Logovi Tété Elom Mike Norbert    
### NIP: 536860599

### Nom complet: Ndjeudji Kuibou Harrol           
### NIP: 537021333


### Liste des codes et descriptions des fonctionnalités sélectionnées:


- (FA21) Intégration de la fonctionnalité de Service Discovery de Consul-Connect ==> 5%
- (FA31) Intégration d’un outil de gestion de journaux (Loki) ==> 5%
- (FA32) Intégration de monitoring des ressources physiques(Prometheus) ==> 5%
- (FA34) Visualisation (Grafana) ==> 10%
- (FA41) Intégration d’un système de Continuous Delivery (ArgoCD) ==> 15% 


### Directives nécessaires à la correction

- Démarrage de notre solution :

    - Installer helm3
    - Créer le cluster via la commande kind create cluster --config cluster.yaml
    - Dans le dossier final_tp_devops exécuter le script:  bash deploy.sh 
    - Patienter environ 5 minutes pour que tout nos services démarrent  car le script installe tout ce qu'il faut pour que notre solution soit lancer avec succès

    # Pour lancer l'application, faire

    - localhost:81 


    ## Pour les fonctionnalités avancées, nous n'avons pas gérer avec l'ingress donc pour chaque fonctionnalité, nous accédons via kubectl port-forward

    # Pour accéder à Grafana (Prometheus, grafana et loki sont déployés ensemble dans notre solution voir le dossier monitoring ) faire:

         - kubectl port-forward svc/prom-grafana -n monitoring 3000:80 ===> username: admin |  password: prom-operator

         - acceder à l'interface graphique dans le navigateur via localhost:3000


    # Accès à argocd:

        - kubectl port-forward svc/argocd-server -n argocd 3002:80

        - acceder à l'interface graphique dans le navigateur via localhost:3002

       # Information de connexion: 

            Username: admin

            Le mot de passe d'argocd s'obtient via la commande :

             - kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d


    # Accès à consul

    - kubectl port-forward svc/consul-ui -n consul 3003:80
        
    - acceder à l'interface graphique dans le navigateur via localhost:3003

### Commentaires généraux:

Dans le dossier submission, nous avons déployer nos différents services api_gateway, frontend, feedback_api en suivant les spécifications de l'énoncé du travail. Ces services fonctionnent très bien. 

Le PersistentVolume et le PersistentVolumeClaim ont été déployés pour le stockage des différents résultats de l'analyseur.

Nous avons configurer un ingress pour exposer notre application via le localhost.

Le dossier monitoring comporte les configurations de notre solution de surveillance et collecte de métriques.
    - Une fois qu'on accède à Grafana, nous pouvons visualiser les métriques collectées par Prometheus et les logs via Loki. Pour ce fait pour les logs, aller sur explore, puis sélectionner loki pour l'affichage des logs de notre système; Dans le champs label filter choisir select label et select value par exemple pod = api_gateway et cliquer sur Run query.

    - Pour Prometheus, faire un scénario identique mais comme explore choisir prometheus, ce qui s'ajoute ce sont les métriques à collecter (cpu,ram) et sélectionner dans le champ label filter le select label  et select value et cliquer sur run query.

Une fois qu'on accède à argocd, cliquer sur submission puis sur sync et synchronise. Nous observons donc le déploiement des états d'application souhaités dans les environnements cibles spécifiés.

### NB: Le dossier annexe comporte les différents résultats des tests de nos fonctionnalités.
### NB: Lien gitlab du projet: https://gitlab.com/MikeLogovi/final_tp_devops

![Screenshot](annexe/admin_feedback_cmd.png)
![Screenshot](annexe/admin_feedback.jpeg)
![Screenshot](annexe/app.jpeg)
![Screenshot](annexe/argoCD.jpeg)
![Screenshot](annexe/Consul.jpeg)
![Screenshot](annexe/loki.jpeg)
![Screenshot](annexe/prometheus.jpeg)